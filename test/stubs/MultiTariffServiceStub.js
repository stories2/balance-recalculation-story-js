export class MultiTariffServiceStub {
    constructor(tariffs) {
        this.tariffs = tariffs;
    }

    getTariffs() {
        return this.tariffs;
    }
}