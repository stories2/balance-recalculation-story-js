export class NoRateTariffStub  {
    constructor(additionalFee) {
        this.additionalFee = additionalFee || 0;
    }

    getAdditionalFee() {
        return this.additionalFee;
    }

    getType() {
        return new NoRateTariffTypeStub();
    }
}

export class NoRateTariffTypeStub {
    isUnitBased() {
        return false;
    }
}

